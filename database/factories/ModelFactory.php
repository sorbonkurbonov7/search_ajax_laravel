<?php

use Faker\Generator as Faker;


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'customerName' => $faker->name,
        'address' => $faker->address,
        'city' => $faker->city,
        'postalCode' => $faker->postcode,
        'country' => $faker->country,
        'customerID' => $faker->buildingNumber,
    ];
});


